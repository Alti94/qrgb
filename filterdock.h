#ifndef FILTERDOCK_H
#define FILTERDOCK_H

#include <QDockWidget>
#include <QVector>
#include "customwindowscreator.h"
#include "tools/binarization.h"

namespace Ui {
class FilterDock;
}

class FilterDock : public QDockWidget
{
    Q_OBJECT

public:
    explicit FilterDock(QWidget *parent = 0);
    ~FilterDock();

private:
    Ui::FilterDock *ui;
    char pointOpt = '+';
    CustomWindowsCreator *windowCreator;

private slots:
    void operChange();
    void on_button_apply_clicked();
    void on_button_median_clicked();
    void on_button_applyLinear_clicked();
    void on_button_customLinear_clicked();
    void customMask(QVector<QVector<int>> mask);
    void on_button_grayscale_clicked();
    void on_button_stretch_clicked();
    void on_button_equalize_clicked();
    void on_button_showHistogram_clicked();

    void on_button_tApply_clicked();

    void on_combo_tMethod_currentIndexChanged(int index);

signals:
    void pointApply(char, qreal, bool, bool, bool);
    void medianApply(int, int);
    void linearApply(QVector<QVector<int>>);
    void toGray();
    void equalize();
    void stretch(int, int);
    void showHistogram();
    void threshold(Binarization::THRESHOLD_METHOD method, int = 0);
};

#endif // FILTERDOCK_H
