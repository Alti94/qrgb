#ifndef TOOLSETTINGS_H
#define TOOLSETTINGS_H

#include <QDockWidget>
#include <ColorPreview>
#include <ColorDialog>

using namespace color_widgets;

namespace Ui {
class ToolSettings;
}

class ToolSettings : public QDockWidget
{
    Q_OBJECT

public:
    explicit ToolSettings(QWidget *parent = 0);
    ~ToolSettings();

    struct Settings
    {
        int width;
        bool filled;
        QColor color;
    };

private:
    Ui::ToolSettings *ui;
    ColorDialog *dialog;

signals:
    void settingsChanged(Settings);


private slots:
    void showColorPicker();
    void setChanges();
};

#endif // TOOLSETTINGS_H
