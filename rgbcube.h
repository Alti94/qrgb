#ifndef RGBCUBE_H
#define RGBCUBE_H

#include <QDialog>

namespace Ui {
class RgbCube;
}

class RgbCube : public QDialog
{
    Q_OBJECT

public:
    explicit RgbCube(QWidget *parent = 0);
    ~RgbCube();

private:
    Ui::RgbCube *ui;
};

#endif // RGBCUBE_H
