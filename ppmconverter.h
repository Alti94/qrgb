#ifndef PPMCONVERTER_H
#define PPMCONVERTER_H

#include <QImage>

class PpmConverter
{
public:
    PpmConverter();
    QImage load(QString filePath, bool *ok = nullptr);

private:
    QImage p6load(QByteArray &fileData, QString &text, bool *ok);
    QImage p3load(QByteArray &fileData, QString &text, bool *ok);
};

#endif // PPMCONVERTER_H
