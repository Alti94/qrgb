#include "savedialog.h"
#include "ui_savedialog.h"
#include <QFileDialog>

SaveDialog::SaveDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SaveDialog)
{
    ui->setupUi(this);
}

SaveDialog::~SaveDialog()
{
    delete ui;
}

void SaveDialog::on_browseButton_clicked()
{
    QString path = QFileDialog::getSaveFileName(this, "Save", QString(), tr("Images (*.jpg)"));
    if (!path.endsWith(".jpg")) path.append(".jpg");
    ui->filePathEdit->setText(path);
}

void SaveDialog::on_qualitySlider_valueChanged(int value)
{
    ui->qualityLabel->setText(QString::number(value));
}

void SaveDialog::on_qualitySlider_rangeChanged(int min, int max)
{

}

void SaveDialog::on_buttonBox_accepted()
{
    emit fileSave(ui->filePathEdit->text(), ui->qualitySlider->value());
}
