#ifndef CUSTOMWINDOWSCREATOR_H
#define CUSTOMWINDOWSCREATOR_H

#include <QDialog>
#include <QVector>

namespace Ui {
class CustomWindowsCreator;
}

class CustomWindowsCreator : public QDialog
{
    Q_OBJECT

public:
    explicit CustomWindowsCreator(QWidget *parent = 0);
    ~CustomWindowsCreator();

private slots:
    void on_button_colAdd_clicked();
    void on_button_rowAdd_clicked();
    void on_button_colRemove_clicked();
    void on_button_rowRemove_clicked();
    void on_buttonBox_accepted();

signals:
    void maskComplete(QVector<QVector<int>>);

private:
    Ui::CustomWindowsCreator *ui;
    int rows = 2;
    int cols = 2;

};

#endif // CUSTOMWINDOWSCREATOR_H
