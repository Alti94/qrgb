#include "dialoghistogram.h"
#include "ui_dialoghistogram.h"

DialogHistogram::DialogHistogram(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogHistogram)
{
    ui->setupUi(this);

    for (int i = 0; i < 256; i++)
    {
        red << 0;
        green << 0;
        blue << 0;
    }

    redSet = new QBarSet("Red");
    greenSet = new QBarSet("Green");
    blueSet = new QBarSet("Blue");

    for (int i = 0; i < 256; i++)
    {
        redSet->append(0);
        greenSet->append(0);
        blueSet->append(0);
    }

    redSet->setColor(QColor(255, 0, 0));
    greenSet->setColor(QColor(0, 255, 0));
    blueSet->setColor(QColor(0, 0, 255));

    redSeries = new QBarSeries();
    greenSeries = new QBarSeries();
    blueSeries = new QBarSeries();

    redSeries->append(redSet);
    greenSeries->append(greenSet);
    blueSeries->append(blueSet);

    redSeries->setBarWidth(1);
    greenSeries->setBarWidth(1);
    blueSeries->setBarWidth(1);

    redChart = new QChart();
    greenChart = new QChart();
    blueChart = new QChart();

    redChart->addSeries(redSeries);
    greenChart->addSeries(greenSeries);
    blueChart->addSeries(blueSeries);

    redChart->createDefaultAxes();
    QAbstractAxis *redAxisX = redChart->axisX();
    redAxisX->setGridLineVisible(false);
    redAxisX->setLabelsVisible(false);
    redAxisX->setLineVisible(false);
    redChart->axisY()->setMin(0);

    greenChart->createDefaultAxes();
    QAbstractAxis *greenAxisX = greenChart->axisX();
    greenAxisX->setGridLineVisible(false);
    greenAxisX->setLabelsVisible(false);
    greenAxisX->setLineVisible(false);
    greenChart->axisY()->setMin(0);

    blueChart->createDefaultAxes();
    QAbstractAxis *blueAxisX = blueChart->axisX();
    blueAxisX->setGridLineVisible(false);
    blueAxisX->setLabelsVisible(false);
    blueAxisX->setLineVisible(false);
    blueChart->axisY()->setMin(0);

    ui->chart_red->setChart(redChart);
    ui->chart_green->setChart(greenChart);
    ui->chart_blue->setChart(blueChart);
}

DialogHistogram::~DialogHistogram()
{
    delete ui;
}

void DialogHistogram::update(QImage &image)
{
    for (int i = 0; i < 256; i++)
        red[i] = green[i] = blue[i] = 0;

    for (int row = 0; row < image.height(); row++)
        for (int col = 0; col < image.width(); col++)
        {
            QColor color = image.pixelColor(col, row);
            red[color.red()] += 1;
            green[color.green()] += 1;
            blue[color.blue()] += 1;
        }

    int redMax = 0, greenMax = 0, blueMax = 0;

    for (int i = 0; i < 256; i++)
    {
        redSet->replace(i, red[i]);
        greenSet->replace(i, green[i]);
        blueSet->replace(i, blue[i]);

        if (redMax < red[i])
            redMax = red[i];
        if (greenMax < green[i])
            greenMax = green[i];
        if (blueMax < blue[i])
            blueMax = blue[i];
    }

    redChart->axisY()->setMax(redMax);
    greenChart->axisY()->setMax(greenMax);
    blueChart->axisY()->setMax(blueMax);
}
