#include "filterdock.h"
#include "ui_filterdock.h"
#include "tools/linearfilter.h"
#include <QRadioButton>
#include <QDoubleSpinBox>
#include <QtDebug>
#include <QComboBox>
#include <QCheckBox>
#include <QLabel>

FilterDock::FilterDock(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::FilterDock)
{
    ui->setupUi(this);

    windowCreator = new CustomWindowsCreator(this);

    ui->combo_linearList->addItem("Smoothing 3x3");
    ui->combo_linearList->addItem("Sharpening 3x3");
    ui->combo_linearList->addItem("Gauss 3x3");
    ui->combo_linearList->addItem("Sobel 3x3");

    ui->combo_tMethod->addItem("Manual");
    ui->combo_tMethod->addItem("Percent Black Selection");
    ui->combo_tMethod->addItem("Mean Iterative Selection");
    ui->combo_tMethod->addItem("Entropy Selection");
    ui->combo_tMethod->addItem("Minimum Error");
    ui->combo_tMethod->addItem("Fuzzy Minimum Error");

    connect(windowCreator, SIGNAL(maskComplete(QVector<QVector<int> >)), this, SLOT(customMask(QVector<QVector<int> >)));
}

FilterDock::~FilterDock()
{
    delete ui;
}


void FilterDock::operChange()
{
    QRadioButton *box = qobject_cast<QRadioButton*>(sender());
    if (box != nullptr)
        pointOpt = box->text().at(0).toLatin1();
}

void FilterDock::on_button_apply_clicked()
{
    emit pointApply(pointOpt, ui->spin_value->value(),
                    ui->check_red->isChecked(),
                    ui->check_green->isChecked(),
                    ui->check_blue->isChecked());
}

void FilterDock::on_button_median_clicked()
{
    emit medianApply(ui->spin_wWidth->value(), ui->spin_wHeight->value());
}

void FilterDock::on_button_applyLinear_clicked()
{
    switch (ui->combo_linearList->currentIndex()) {
    case 0:
        emit linearApply(LinearFilter::SMOOTHING_3X3);
        break;
    case 1:
        emit linearApply(LinearFilter::SHARPENING_3X3);
        break;
    case 2:
        emit linearApply(LinearFilter::GAUSS_3X3);
        break;
    case 3:
        emit linearApply(LinearFilter::SOBEL_3X3);
        break;
    default:
        break;
    }
}

void FilterDock::on_button_customLinear_clicked()
{
    windowCreator->show();
}

void FilterDock::customMask(QVector<QVector<int>> mask)
{
    emit linearApply(mask);
}

void FilterDock::on_button_grayscale_clicked()
{
    emit toGray();
}

void FilterDock::on_button_stretch_clicked()
{
    int start = ui->spin_stretchStart->value();
    int end = ui->spin_stretchEnd->value();
    emit stretch(start, end);
}

void FilterDock::on_button_equalize_clicked()
{
    emit equalize();
}

void FilterDock::on_button_showHistogram_clicked()
{
    emit showHistogram();
}

void FilterDock::on_button_tApply_clicked()
{
    int idx = ui->combo_tMethod->currentIndex();
    auto method = static_cast<Binarization::THRESHOLD_METHOD>(idx);

    emit threshold(method, ui->spin_valueThresh->value());
}

void FilterDock::on_combo_tMethod_currentIndexChanged(int index)
{
    switch (index)
    {
    case 0:
        ui->label_thresh->setVisible(true);
        ui->spin_valueThresh->setVisible(true);
        ui->spin_valueThresh->setMaximum(255);
        ui->spin_valueThresh->setValue(128);
        ui->spin_valueThresh->setSuffix("");
        break;
    case 1:
        ui->label_thresh->setVisible(true);
        ui->spin_valueThresh->setVisible(true);
        ui->spin_valueThresh->setMaximum(255);
        ui->spin_valueThresh->setValue(50);
        ui->spin_valueThresh->setSuffix("%");
        break;
    default:
        ui->label_thresh->setVisible(false);
        ui->spin_valueThresh->setVisible(false);
        break;
    }
}
