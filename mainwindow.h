#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QVector>
#include <QImage>

#include "tools/shape.h"
#include "toolsettings.h"
#include "filterdock.h"
#include "dialoghistogram.h"
#include "tools/binarization.h"
#include "tools/beziercurve.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    enum Tool {
        LINE,
        ELIPSE,
        RECTANGLE,
        MOVE,
        CURVE,
        MODIFY
    };

private slots:
    void toolSelect(QAction *action);
    void mouseCanvasMoved(QMouseEvent *event);
    void mouseCanvasPressed(QMouseEvent *event);
    void on_actionNew_triggered();
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionHistogram_triggered();
    void saveFile(QString path, int q);
    void on_actionIdk_triggered();
    void on_actionRgb_cube_triggered();
    void applyPointTransform(char opt, qreal value, bool red, bool green, bool blue);
    void applyMedianFilter(int windowWidth, int windowHeight);
    void applyLinearFilter(QVector<QVector<int>> mask);
    void toGrayscale();
    void onUpdateCanvas();
    void stretchHist(int start, int end);
    void equalizeHist();
    void threshold(Binarization::THRESHOLD_METHOD method, int value);

private:
    Ui::MainWindow *ui;
    Tool selectedTool = LINE;
    Shape *shape;
    QGraphicsScene *scene;
    QFileDialog fileDialog;
    ToolSettings *toolSettings;
    FilterDock *filterDock;
    DialogHistogram *dialogHistogram;
    BezierCurve *curve;

    void createNewScene(int width, int height);
    QImage toImage();
};

#endif // MAINWINDOW_H
