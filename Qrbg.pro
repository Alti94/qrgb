#-------------------------------------------------
#
# Project created by QtCreator 2017-10-14T14:21:15
#
#-------------------------------------------------

!include( ./colorPickers/color_widgets.pri ) {
    error( "Couldn't find the colorPickers/color_widgets.pri file!" )
}

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Qrbg
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    canvas.cpp \
    tools/shape.cpp \
    tools/rectangle.cpp \
    tools/elipse.cpp \
    tools/line.cpp \
    ppmconverter.cpp \
    savedialog.cpp \
    toolsettings.cpp \
    rgbcube.cpp \
    cubegl.cpp \
    tools/pointtransform.cpp \
    filterdock.cpp \
    tools/linearfilter.cpp \
    tools/otherfilter.cpp \
    customwindowscreator.cpp \
    tools/grayscaleconv.cpp \
    dialoghistogram.cpp \
    tools/histogramoper.cpp \
    tools/binarization.cpp \
    tools/beziercurve.cpp \
    tools/beziermodifierpoint.cpp

HEADERS += \
        mainwindow.h \
    canvas.h \
    tools/shape.h \
    tools/rectangle.h \
    tools/line.h \
    tools/elipse.h \
    ppmconverter.h \
    savedialog.h \
    toolsettings.h \
    rgbcube.h \
    cubegl.h \
    tools/pointtransform.h \
    filterdock.h \
    tools/linearfilter.h \
    tools/otherfilter.h \
    customwindowscreator.h \
    tools/grayscaleconv.h \
    dialoghistogram.h \
    tools/histogramoper.h \
    tools/binarization.h \
    tools/beziercurve.h \
    tools/beziermodifierpoint.h

FORMS += \
        mainwindow.ui \
    savedialog.ui \
    toolsettings.ui \
    rgbcube.ui \
    filterdock.ui \
    customwindowscreator.ui \
    dialoghistogram.ui

DISTFILES +=
