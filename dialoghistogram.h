#ifndef DIALOGHISTOGRAM_H
#define DIALOGHISTOGRAM_H

#include <QDialog>
#include <QtCharts>
#include <QImage>
#include <QVector>
using namespace QtCharts;

namespace Ui {
class DialogHistogram;
}

class DialogHistogram : public QDialog
{
    Q_OBJECT

public:
    explicit DialogHistogram(QWidget *parent = 0);
    void update(QImage &image);
    ~DialogHistogram();

private:
    Ui::DialogHistogram *ui;
    QVector<int> red, green, blue;

    QBarSet *redSet, *greenSet, *blueSet;
    QBarSeries *redSeries, *greenSeries, *blueSeries;
    QChart *redChart, *greenChart, *blueChart;
};

#endif // DIALOGHISTOGRAM_H
