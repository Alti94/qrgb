#include "toolsettings.h"
#include "ui_toolsettings.h"
#include <QSlider>
#include <QCheckBox>

ToolSettings::ToolSettings(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::ToolSettings),
    dialog(new ColorDialog(this))
{
    ui->setupUi(this);

    dialog->setAlphaEnabled(false);
    dialog->setColor(Qt::black);
    ui->color_preview->setColor(Qt::black);

    connect(dialog, SIGNAL(colorSelected(QColor)), ui->color_preview, SLOT(setColor(QColor)));
}

ToolSettings::~ToolSettings()
{
    delete ui;
}

void ToolSettings::showColorPicker()
{
    dialog->show();
}

void ToolSettings::setChanges()
{
    Settings settings;
    settings.width = ui->slide_width->value();
    settings.color = ui->color_preview->color();
    settings.filled = ui->check_fill->isChecked();
    emit settingsChanged(settings);
}
