#include "canvas.h"
#include <QtDebug>

Canvas::Canvas(QWidget *parent) : QGraphicsView(parent)
{

}

void Canvas::mouseMoveEvent(QMouseEvent *event)
{
    QGraphicsView::mouseMoveEvent(event);
    emit mouseMoved(event);
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
    QGraphicsView::mousePressEvent(event);
    emit mousePressed(event);
}
