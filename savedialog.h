#ifndef SAVEDIALOG_H
#define SAVEDIALOG_H

#include <QDialog>

namespace Ui {
class SaveDialog;
}

class SaveDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SaveDialog(QWidget *parent = 0);
    ~SaveDialog();

signals:
    void fileSave(QString path, int q);

private slots:
    void on_browseButton_clicked();

    void on_qualitySlider_rangeChanged(int min, int max);

    void on_qualitySlider_valueChanged(int value);

    void on_buttonBox_accepted();

private:
    Ui::SaveDialog *ui;
};

#endif // SAVEDIALOG_H
