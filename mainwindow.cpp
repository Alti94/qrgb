#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QMouseEvent>
#include <QPainter>
#include <QtDebug>
#include <QMessageBox>

#include <ColorDialog>

#include "tools/rectangle.h"
#include "tools/line.h"
#include "tools/elipse.h"
#include "tools/pointtransform.h"
#include "tools/linearfilter.h"
#include "tools/otherfilter.h"
#include "tools/grayscaleconv.h"
#include "tools/histogramoper.h"
#include "ppmconverter.h"
#include "savedialog.h"
#include "rgbcube.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->graphicsView->setVisible(false);
    scene = NULL;
    curve = Q_NULLPTR;

    toolSettings = new ToolSettings();
    filterDock = new FilterDock();
    dialogHistogram = new DialogHistogram();
    addDockWidget(Qt::RightDockWidgetArea, toolSettings, Qt::Vertical);
    addDockWidget(Qt::RightDockWidgetArea, filterDock, Qt::Vertical);


    connect(ui->mainToolBar, SIGNAL(actionTriggered(QAction*)), this, SLOT(toolSelect(QAction*)));
    connect(ui->graphicsView, SIGNAL(mousePressed(QMouseEvent*)), this, SLOT(mouseCanvasPressed(QMouseEvent*)));
    connect(ui->graphicsView, SIGNAL(mouseMoved(QMouseEvent*)), this, SLOT(mouseCanvasMoved(QMouseEvent*)));

    connect(filterDock, SIGNAL(pointApply(char,qreal,bool,bool,bool)), this, SLOT(applyPointTransform(char,qreal,bool,bool,bool)));
    connect(filterDock, SIGNAL(medianApply(int,int)), this, SLOT(applyMedianFilter(int,int)));
    connect(filterDock, SIGNAL(linearApply(QVector<QVector<int> >)), this, SLOT(applyLinearFilter(QVector<QVector<int> >)));
    connect(filterDock, SIGNAL(toGray()), this, SLOT(toGrayscale()));
    connect(filterDock, SIGNAL(stretch(int,int)), this, SLOT(stretchHist(int,int)));
    connect(filterDock, SIGNAL(equalize()), this, SLOT(equalizeHist()));
    connect(filterDock, SIGNAL(showHistogram()), this, SLOT(on_actionHistogram_triggered()));
    connect(filterDock, SIGNAL(threshold(Binarization::THRESHOLD_METHOD,int)), this, SLOT(threshold(Binarization::THRESHOLD_METHOD,int)));
}

MainWindow::~MainWindow()
{
    delete toolSettings;
    delete filterDock;
    delete scene;
    delete ui;
}

void MainWindow::toolSelect(QAction *action)
{
    QList<QAction *> toolbar = ui->mainToolBar->actions();
    quint8 size = toolbar.size();
    for (quint8 i = 0; i < size; i++)
    {
        toolbar.at(i)->setChecked(false);
        if (toolbar.at(i)->text() == action->text())
            selectedTool = static_cast<Tool>(i);
    }
    action->setChecked(true);
    qInfo("%d", selectedTool);

    if (selectedTool == MOVE)
    {
        for (int i = 0; i < scene->items().size(); i++)
        {
            QGraphicsItem *item = scene->items().at(i);
            if (item->type() > QGraphicsItem::UserType)
            {
                item->setFlag(QGraphicsItem::ItemIsMovable, true);
            }
            if (item->type() == BezierCurve::Type)
            {
                static_cast<BezierCurve*>(item)->setEditVisible(false);
            }
        }
    }
    else if (selectedTool == MODIFY)
    {
        for (int i = 0; i < scene->items().size(); i++)
        {
            QGraphicsItem *item = scene->items().at(i);
            if (item->type() == BezierCurve::Type)
            {
                static_cast<BezierCurve*>(item)->setEditVisible(true);
            }
            if (item->type() > QGraphicsItem::UserType)
            {
                item->setFlag(QGraphicsItem::ItemIsMovable, false);
            }
        }
    }
    else
    {
        for (int i = 0; i < scene->items().size(); i++)
        {
            QGraphicsItem *item = scene->items().at(i);
            if (item->type() > QGraphicsItem::UserType)
            {
                item->setFlag(QGraphicsItem::ItemIsMovable, false);
            }
            if (item->type() == BezierCurve::Type)
            {
                static_cast<BezierCurve*>(item)->setEditVisible(false);
            }
        }
    }
}

void MainWindow::mouseCanvasMoved(QMouseEvent *event)
{
    if (selectedTool != MOVE && selectedTool != CURVE && selectedTool != MODIFY)
    {
        shape->resize(event->x(), event->y());
        ui->graphicsView->scene()->update();
    }
}

void MainWindow::mouseCanvasPressed(QMouseEvent *event)
{
    if (selectedTool == CURVE)
    {
        if (event->button() == Qt::LeftButton)
            if (curve == Q_NULLPTR)
            {
                curve = new BezierCurve(event->x(), event->y());
                scene->addItem(curve);
            }
            else
                curve->addPoint(event->x(), event->y());
        else if (event->button() == Qt::RightButton)
            curve = Q_NULLPTR;
    }
    else if (selectedTool != MOVE && selectedTool != MODIFY)
    {
        curve = Q_NULLPTR;
        switch (selectedTool) {
        case LINE:
            shape = new Line(event->x(), event->y());
            break;
        case ELIPSE:
            shape = new Elipse(event->x(), event->y());
            break;
        case RECTANGLE:
            shape = new Rectangle(event->x(), event->y());
            break;
        default:
            shape = new Line(event->x(), event->y());
        }

        scene->addItem(shape);
    }
}

void MainWindow::on_actionNew_triggered()
{
    int width = 1000;
    int height = 1000;

    createNewScene(width, height);
}

void MainWindow::on_actionOpen_triggered()
{
    QString filePath = QFileDialog::getOpenFileName(this, "Open", QString(), tr("Images (*.ppm *.jpg *.png)"));
    if (!filePath.isEmpty())
    {
        QImage image;
        if (filePath.endsWith(".jpg", Qt::CaseInsensitive) || filePath.endsWith(".png", Qt::CaseInsensitive))
        {
            image = QImage(filePath);
        }
        else
        {
//            PpmConverter conv;
//            image = conv.load(filePath);
            image = QImage(filePath);
        }

        if (!image.isNull())
        {
            createNewScene(image.width(), image.height());
            scene->addPixmap(QPixmap::fromImage(image));
        }
        else
        {
            QMessageBox::critical(this, "Load error!", "File cannot be loaded!");
        }
    }
}

void MainWindow::on_actionSave_triggered()
{
    SaveDialog *dialog = new SaveDialog(this);
    dialog->setAttribute(Qt::WA_DeleteOnClose);

    connect(dialog, SIGNAL(fileSave(QString,int)), this, SLOT(saveFile(QString,int)));
    dialog->show();
}

void MainWindow::saveFile(QString path, int q)
{
    QImage image(scene->width(), scene->height(), QImage::Format_RGB32);
    QPainter painter(&image);
    scene->render(&painter);

    if (!image.save(path, "JPG", q))
        QMessageBox::critical(this, "Save error!", "File cannot be written!");
}

void MainWindow::on_actionIdk_triggered()
{
    QImage image(scene->width(), scene->height(), QImage::Format_RGB32);
    QPainter painter(&image);
    scene->render(&painter);

    int maxR = 0;
    int maxG = 0;
    int maxB = 0;
    int red;
    int green;
    int blue;

    for (int height = 0; height < image.height(); height++)
        for (int width = 0; width < image.width(); width++)
        {
            image.pixelColor(width, height).getRgb(&red, &green, &blue);
            if (red > maxR) maxR = red;
            if (green > maxG) maxG = green;
            if (blue > maxB) maxB = blue;
        }

    for (int height = 0; height < image.height(); height++)
        for (int width = 0; width < image.width(); width++)
        {
            image.pixelColor(width, height).getRgb(&red, &green, &blue);

            image.setPixelColor(width, height,
                                QColor::fromRgb(
                                    (int)(((qreal)red / maxR) * 255),
                                    (int)(((qreal)green / maxG) * 255),
                                    (int)(((qreal)blue / maxB) * 255))
                                );
        }

    scene->addPixmap(QPixmap::fromImage(image));
}

void MainWindow::on_actionRgb_cube_triggered()
{
    RgbCube *dialog = new RgbCube(this);
    dialog->setAttribute(Qt::WA_DeleteOnClose);
    dialog->show();
}

void MainWindow::applyPointTransform(char opt, qreal value, bool red, bool green, bool blue)
{
    QImage imageBefore = toImage();

    QImage imageAfter = PointTransform::transform(imageBefore, opt, value, red, green, blue);
    scene->items().clear();
    scene->addPixmap(QPixmap::fromImage(imageAfter));
}

void MainWindow::applyMedianFilter(int windowWidth, int windowHeight)
{
    QImage imageBefore = toImage();

    QImage imageAfter = OtherFilter::medianFilter(imageBefore, windowWidth, windowHeight);
    scene->items().clear();
    scene->addPixmap(QPixmap::fromImage(imageAfter));
}

void MainWindow::applyLinearFilter(QVector<QVector<int>> mask)
{
    QImage imageBefore = toImage();

    QImage imageAfter = LinearFilter::filter(imageBefore, mask);
    scene->items().clear();
    scene->addPixmap(QPixmap::fromImage(imageAfter));
}

void MainWindow::toGrayscale()
{
    QImage imageBefore = toImage();

    QImage imageAfter = GrayscaleConv::toGrayscale1(imageBefore);
    scene->items().clear();
    scene->addPixmap(QPixmap::fromImage(imageAfter));
}

void MainWindow::on_actionHistogram_triggered()
{
    if (!dialogHistogram->isVisible())
    {
        QImage image = toImage();
        dialogHistogram->update(image);
        dialogHistogram->show();
    }
}

void MainWindow::onUpdateCanvas()
{
    if (dialogHistogram->isVisible())
    {
        QImage image = toImage();
        dialogHistogram->update(image);
    }
}

void MainWindow::createNewScene(int width, int height)
{
    if (scene != nullptr)
        delete scene;

    scene = new QGraphicsScene(0, 0, width, height);
    scene->setBackgroundBrush(QBrush(Qt::white));

    ui->graphicsView->setScene(scene);
    ui->graphicsView->setFixedSize(width, height);
    ui->graphicsView->setVisible(true);

    connect(scene, SIGNAL(changed(QList<QRectF>)), this, SLOT(onUpdateCanvas()));
}

QImage MainWindow::toImage()
{
    QImage image(scene->width(), scene->height(), QImage::Format_RGB32);
    QPainter painter(&image);
    scene->render(&painter);

    return image;
}

void MainWindow::stretchHist(int start, int end)
{
    QImage imageBefore = toImage();

    QImage imageAfter = HistogramOper::stretch(imageBefore, start, end);
    scene->items().clear();
    scene->addPixmap(QPixmap::fromImage(imageAfter));
}

void MainWindow::equalizeHist()
{
    QImage imageBefore = toImage();

    QImage imageAfter = HistogramOper::equation(imageBefore);
    scene->items().clear();
    scene->addPixmap(QPixmap::fromImage(imageAfter));
}

void MainWindow::threshold(Binarization::THRESHOLD_METHOD method, int value)
{
    QImage imageBefore = toImage();

    QImage imageAfter = Binarization::thresholdImage(imageBefore, method, value);
    scene->items().clear();
    scene->addPixmap(QPixmap::fromImage(imageAfter));
}
