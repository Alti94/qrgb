#include "ppmconverter.h"
#include <Qt>
#include <QFile>
#include <QRegularExpression>
#include <QString>


PpmConverter::PpmConverter()
{

}

QImage PpmConverter::load(QString filePath, bool *ok)
{
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return QImage();

    QByteArray fileData = file.readAll();
    QString text(fileData);
    text = text.remove(QRegularExpression("#.*\n"));
    text = text.simplified();

    if (ok != nullptr) (*ok) = true;

    if (text.startsWith("P3"))
        return p3load(fileData, text, ok);
    else if (text.startsWith("P6"))
        return p6load(fileData, text, ok);
    else
        if (ok != nullptr) (*ok) = false;


    return QImage();
}

QImage PpmConverter::p3load(QByteArray &fileData, QString &text, bool *ok)
{
    if (!text.contains(QRegularExpression("^(P3 [0-9]+ [0-9]+ [0-9]+).*$")))
    {
        if (ok != nullptr) (*ok) = false;
        return QImage(); // err
    }

    QVector<QStringRef> dataVector = text.splitRef(' ', QString::SkipEmptyParts);

    int width = dataVector.at(1).toInt();
    int height = dataVector.at(2).toInt();
    int depth = dataVector.at(3).toInt();

    if ((depth != 0xff && depth != 0xffff) || width <= 0 || height <= 0)
    {
        if (ok != nullptr) (*ok) = false;
        return QImage(); // err
    }

    if (dataVector.size() != width * height * 3 + 4)
    {
        if (ok != nullptr) (*ok) = false;
        return QImage(); // err
    }

    unsigned char *imageData = new unsigned char[width * height * 4];

    if (depth == 0xff)
    {
        for (int i = 0; i < width * height; i++)
        {
            imageData[i * 4 + 0] = (unsigned char) dataVector.at(i * 3 + 6).toUInt();
            imageData[i * 4 + 1] = (unsigned char) dataVector.at(i * 3 + 5).toUInt();
            imageData[i * 4 + 2] = (unsigned char) dataVector.at(i * 3 + 4).toUInt();
            imageData[i * 4 + 3] = 0xff;
        }
    }
    else
    {
        for (int i = 0; i < width * height; i++)
        {
            imageData[i * 4 + 0] = (unsigned char) (dataVector.at(i * 3 + 6).toUInt() / 65535.0 * 255);
            imageData[i * 4 + 1] = (unsigned char) (dataVector.at(i * 3 + 5).toUInt() / 65535.0 * 255);
            imageData[i * 4 + 2] = (unsigned char) (dataVector.at(i * 3 + 4).toUInt()  / 65535.0 * 255);
            imageData[i * 4 + 3] = 0xff;
        }
    }

    return QImage(imageData, width, height, QImage::Format_RGB32);
}

QImage PpmConverter::p6load(QByteArray &fileData, QString &text, bool *ok)
{
    text.replace(QRegularExpression("^(P6 [0-9]+ [0-9]+ [0-9]+).*$"), "\\1");
    QStringList params = text.split(' ', QString::SkipEmptyParts);

    int a = fileData.size();

    if (params.size() != 4)
    {
        if (ok != nullptr) (*ok) = false;
        return QImage(); // err
    }

    int width = params.at(1).toInt();
    int height = params.at(2).toInt();
    int depth = params.at(3).toInt();

    if (depth != 255 || width <= 0 || height <= 0)
    {
        if (ok != nullptr) (*ok) = false;
        return QImage(); // err
    }

    QByteArray data = fileData.right(width * height * 3);

    if (data.size() != width * height * 3)
    {
        if (ok != nullptr) (*ok) = false;
        return QImage(); // err
    }

    unsigned char *imageData = new unsigned char[width * height * 4];

    for (int i = 0; i < width * height; i++)
    {
        imageData[i * 4 + 0] = (unsigned char) data.at(i * 3 + 2);
        imageData[i * 4 + 1] = (unsigned char) data.at(i * 3 + 1);
        imageData[i * 4 + 2] = (unsigned char) data.at(i * 3);
        imageData[i * 4 + 3] = 0xff;
    }

    return QImage(imageData, width, height, QImage::Format_RGB32);
}
