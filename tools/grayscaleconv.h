#ifndef GRAYSCALECONV_H
#define GRAYSCALECONV_H

#include <QObject>
#include <QImage>

class GrayscaleConv : public QObject
{
    Q_OBJECT
public:
    explicit GrayscaleConv(QObject *parent = nullptr);
    static QImage toGrayscale1(QImage &image);

signals:

public slots:
};

#endif // GRAYSCALECONV_H
