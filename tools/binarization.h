#ifndef BINARIZATION_H
#define BINARIZATION_H

#include <QObject>
#include <QImage>
#include <QVector>

class Binarization : public QObject
{
    Q_OBJECT
public:
    enum THRESHOLD_METHOD {
        MANUAL,
        PERCENT_BLACK_SELECTION,
        MEAN_ITERATIVE_SELECTION,
        ENTROPY_SELECTION,
        MINIMUM_ERROR,
        FUZZY_MINIMUM_ERROR
    };

    explicit Binarization(QObject *parent = nullptr);
    static QImage thresholdImage(QImage &image, THRESHOLD_METHOD method, int value = 0);

private:
    static QImage toGrayScale(QImage &image);
    static QVector<int> makeHistoram(QImage &image);
    static int percentBlackThreshold(QImage &image, int percent);
    static int meanIterativeSelection(QImage &image);
    static int entropySelection(QImage &image);
    static int minimumError(QImage &image);
    static int fuzzyMinimumError(QImage &image);
    static qreal getLog(qreal f);
    static qreal shannon(qreal x);

signals:

public slots:
};

#endif // BINARIZATION_H
