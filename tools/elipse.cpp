#include "elipse.h"

#include <QPainter>
#include <QtDebug>

Elipse::Elipse(int x, int y)
{
    this->x = x;
    this->y = y;
}

void Elipse::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPen pen(Qt::RoundCap);
    pen.setWidth(this->lineWidth);
    pen.setColor(this->color);
    painter->setPen(pen);
    QBrush brush;
    if (this->fill)
        brush = QBrush(this->color);
    else
        brush = QBrush();
    painter->setBrush(brush);
    painter->drawEllipse(this->x, this->y, this->width, this->height);
}

QRectF Elipse::boundingRect() const
{
    return QRectF(this->x, this->y, this->width, this->height).normalized();
}

void Elipse::resize(int width, int height)
{
    this->width = width - this->x;
    this->height = height - this->y;
}
