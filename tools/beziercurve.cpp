#include "beziercurve.h"

#include <QPointF>
#include <QtMath>
#include <QPainter>
#include "beziermodifierpoint.h"

BezierCurve::BezierCurve(int x, int y) :
    BezierCurve(QPointF(x,y))
{
}

BezierCurve::BezierCurve(const QPointF &point) :
//    guidePoints(QVector()),
    pointPen(Qt::red),
    pointBrush(Qt::red),
    guideLinePen(Qt::gray),
    editVisible(false)
{
    guideLinePen.setStyle(Qt::DashLine);
    BezierModifierPoint *modPoint = new BezierModifierPoint(point, this);
    guidePoints << modPoint;
    modPoint->setVisible(editVisible);
    //setFlag(QGraphicsItem::ItemIsMovable, true);
    update();
}

BezierCurve::~BezierCurve()
{
    while (!childItems().isEmpty())
        delete childItems().takeLast();
}

void BezierCurve::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawPolyline(curve.data(), curve.size());
}

QRectF BezierCurve::boundingRect() const
{
    return rect;
}

void BezierCurve::addPoint(int x, int y)
{
    addPoint(QPointF(x, y));
}

void BezierCurve::addPoint(QPointF point)
{
//    guidePoints.push_back(point);
    QGraphicsLineItem *line = new QGraphicsLineItem(guidePoints.last()->getPos().x(), guidePoints.last()->getPos().y(), point.x(), point.y(), this);
    line->setPen(guideLinePen);
    BezierModifierPoint *modPoint = new BezierModifierPoint(point, this);
    guidePoints << modPoint;
    guideLines << line;

    line->setVisible(editVisible);
    modPoint->setVisible(editVisible);
    update();
}

void BezierCurve::removePoint(int pointNum)
{
    if (pointNum < 0 || pointNum >= guidePoints.size())
        return;

    QGraphicsLineItem *line;
    if (pointNum == guidePoints.size() - 1)
        line = guideLines.takeLast();
    else
        line = guideLines.takeAt(pointNum);

    childItems().removeOne(line);
    delete line;

    BezierModifierPoint *point = guidePoints.takeAt(pointNum);
    childItems().removeOne(point);
    delete point;

    update();
}

void BezierCurve::modPoint()
{
    update();
}

void BezierCurve::setEditVisible(bool editVisible)
{
    this->editVisible = editVisible;
    for (QGraphicsItem *item : childItems())
        item->setVisible(editVisible);
}

void BezierCurve::update()
{
    curve.clear();

    QVector<int> strongs;
    int n = guidePoints.size() - 1;
//    QPen pen;
//    pen.setColor(Qt::red);

    for (int i = 0; i < n; i++)
    {
        guideLines[i]->setLine(guidePoints[i]->getPos().x(), guidePoints[i]->getPos().y(), guidePoints[i + 1]->getPos().x(), guidePoints[i + 1]->getPos().y());
    }

    for (int i = 0; i <= n; i++)
    {
        strongs.push_back(combinations(n, i));
    }

    for (qreal t = 0; t <= 1; t += interval)
    {
        qreal x = 0, y = 0;
        for (int i = 0; i <= n; i++)
        {
            qreal multip = strongs[i] * qPow(1 - t, n - i) * qPow(t, i);
            x += multip * guidePoints[i]->getPos().x();
            y += multip * guidePoints[i]->getPos().y();
        }

        curve.push_back(QPointF(x, y));
    }

    prepareGeometryChange();
    updateRect();
    QGraphicsItem::update(rect);
}

void BezierCurve::updateRect()
{
    rect.setRect(0,0,0,0);

    for (QPointF &p : curve) {
        if (p.x() <= rect.left())
            rect.setLeft(p.x());
        else if (p.x() >= rect.right())
            rect.setRight(p.x());

        if (p.y() <= rect.top())
            rect.setTop(p.y());
        else if (p.y() >= rect.bottom())
            rect.setBottom(p.y());
    }
}

int BezierCurve::combinations(int n, int k)
{
    if (k == 0 || n == k)
        return 1;
    if (k == 1)
        return n;
    if (k > n)
        return 0;

    return strong(n) / (strong(k) * strong(n - k));
}

int BezierCurve::strong(int k)
{
    int tmp = k;
    int val = 1;
    while (tmp > 1)
    {
        val *= tmp;
        --tmp;
    }

    return val;
}
