#include "pointtransform.h"
#include <QRgb>

PointTransform::PointTransform(QObject *parent) : QObject(parent)
{

}

QImage PointTransform::transform(QImage &img, char oper, qreal val, bool red, bool green, bool blue)
{
    switch (oper) {
    case '+':
        return addRemove(img, val, red, green, blue);
    case '-':
        return addRemove(img, -val, red, green, blue);
    case '*':
        return multiplyDivide(img, val, red, green, blue);
    case '/':
        return multiplyDivide(img, 1/val, red, green, blue);
    default:
        return img;
    }
}

QImage PointTransform::addRemove(QImage &img, qreal val, bool red, bool green, bool blue)
{
    qint32 LUT[256];
    for (int i = 0; i < 256; i++)
    {
        int tmp = i + val;
        LUT[i] = (tmp > 255) ? 255 : ((tmp < 0) ? 0 : tmp);
    }

    return lutApply(img, LUT, red, green, blue);
}

QImage PointTransform::multiplyDivide(QImage &img, qreal val, bool red, bool green, bool blue)
{
    qint32 LUT[256];
    for (int i = 0; i < 256; i++)
    {
        int tmp = i * val;
        LUT[i] = (tmp > 255) ? 255 : ((tmp < 0) ? 0 : tmp);
    }

    return lutApply(img, LUT, red, green, blue);
}

QImage PointTransform::lutApply(QImage img, qint32 (&lut)[256], bool red, bool green, bool blue)
{
    for (int y = 0;  y < img.height(); y++)
        for (int x = 0; x < img.width(); x++)
        {
            QColor color = img.pixelColor(x, y);
            if (red)
                color.setRed(lut[color.red()]);
            if (green)
                color.setGreen(lut[color.green()]);
            if (blue)
                color.setBlue(lut[color.blue()]);
//            color.setRgb(lut[color.red()], lut[color.green()], lut[color.blue()]);
            img.setPixelColor(x, y, color);
        }

    return img;
}
