#ifndef LINE_H
#define LINE_H

#include "tools/shape.h"

class Line : public Shape
{
public:
    enum { Type = UserType + 1 };
    Line(int x, int y);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
    void resize(int width, int height);

    int type() const
    {
        return Type;
    }

private:
};

#endif // LINE_H
