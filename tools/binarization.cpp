#include "binarization.h"
#include "grayscaleconv.h"
#include <QtMath>

Binarization::Binarization(QObject *parent) : QObject(parent)
{

}

QImage Binarization::toGrayScale(QImage &image)
{
    bool isGrayScale = true;
    for (int row = 0; row < image.height(); row++)
    {
        for (int col = 0; col < image.width(); col++)
        {
            QColor color = image.pixelColor(col, row);
            if (color.red() != color.green() || color.red() != color.blue() || color.green() != color.blue())
            {
                isGrayScale = false;
                break;
            }
        }
        if (!isGrayScale)
            break;
    }

    if (isGrayScale)
        return image;

    return GrayscaleConv::toGrayscale1(image);
}

QImage Binarization::thresholdImage(QImage &image, THRESHOLD_METHOD method, int value)
{
    image = toGrayScale(image);

    int threshold;

    switch (method) {
    case MANUAL:
        threshold = value;
        break;
    case PERCENT_BLACK_SELECTION:
        threshold = percentBlackThreshold(image, value);
        break;
    case MEAN_ITERATIVE_SELECTION:
        threshold = meanIterativeSelection(image);
        break;
    case ENTROPY_SELECTION:
        threshold = entropySelection(image);
        break;
    case MINIMUM_ERROR:
        threshold = minimumError(image);
        break;
    case FUZZY_MINIMUM_ERROR:
        threshold = fuzzyMinimumError(image);
        break;
    default:
        threshold = value;
        break;
    }

    for (int row = 0; row < image.height(); row++)
        for (int col = 0; col < image.width(); col++)
        {
            QColor color = image.pixelColor(col, row);
            if (color.red() > threshold)
                image.setPixelColor(col, row, Qt::white);
            else
                image.setPixelColor(col, row, Qt::black);
        }

    return image;
}

QVector<int> Binarization::makeHistoram(QImage &image)
{
    QVector<int> histogram;
    for (int i = 0; i < 256; i++)
    {
        histogram << 0;
    }

    for (int row = 0; row < image.height(); row++)
        for (int col = 0; col < image.width(); col++)
        {
            QColor color = image.pixelColor(col, row);
            histogram[color.red()] += 1;
        }

    return histogram;
}

int Binarization::percentBlackThreshold(QImage &image, int percent)
{
    QVector<int> histogram = makeHistoram(image);

    int pixelNumber =(qreal) (image.width() * image.height()) * ((qreal)percent / 100.0);
    int count = 0;

    int i = 0;
    for (; i < 256; i++)
    {
        count += histogram[i];

        if (count > pixelNumber)
            break;
    }

    return (i < 255) ? i : 255;
}

int Binarization::meanIterativeSelection(QImage &image)
{
    int tk = 0, tkm1 = 0;
    int a = 0, b = 0, c = 0, d = 0;
    int h = 0;
    int histAvgValue = 0;

    QVector<int> histogram = makeHistoram(image);

    for (int i = 0; i < 256; i++) {
        histAvgValue += i * histogram[i];
    }

    histAvgValue = (qreal) histAvgValue / (qreal) (image.width() * image.height());

    tk = histAvgValue;

    do {
        tkm1 = tk;
        a = b = c = d = 0;

        for (int i = 0; i <= tkm1; i++) {
            h = histogram[i];
            a += i * h;
            b += h;
        }

        b += b;

        for (int i = tkm1 + 1; i < 256; i++) {
            h = histogram[i];
            c += i * h;
            d += h;
        }
        d += d;
        tk = (qreal) a / (qreal) b + (qreal) c / (qreal) d;
    } while (tk != tkm1);

    return tk;
}

int Binarization::entropySelection(QImage &image)
{
    return 0;
}

int Binarization::minimumError(QImage &image)
{
    int threshold = 0;
    qreal minvalue = std::numeric_limits<qreal>::max();
    qreal J  = 0;
    qreal s1 = 0, s2 = 0;
    qreal fv = 0;
    qreal u1 = 0, u2 = 0;
    qreal v = 0;
    qreal P1 = 0, P2 = 0;
    qreal Pi1 = 0, Pi2 = 0;

    QVector<int> histogram = makeHistoram(image);

    for (int i = 0; i < 256; i++) {
        v = histogram[i];
        P2 += v;
        v *= i;
        Pi2 += v;
    }

    for (int i = 0; i < 256; i++) {
        v = histogram[i];
        P1 += v;
        P2 -= v;
        v *= i;
        Pi1 += v;
        Pi2 -= v;
        u1 = (P1 > 0) ? Pi1/P1 : 0;
        u2 = (P2 > 0) ? Pi2/P2 : 0;

        if (P1 > 0) {
            for (int j = 0; j <= i; j++) {
                fv = j - u1;
                s1 += fv * fv * histogram[j];
            }
            s1 /= P1;
        }

        if (P2 > 0) {
            for (int k = i + 1; k < 256; k++) {
                fv = k - u2;
                s2 += fv * fv * histogram[k];
            }
            s2 /= P2;
        }

        J = 1 + 2 * (P1 * (getLog(s1) - getLog(P1)) + P2 * (getLog(s2) - getLog(P2)));
        if (J < minvalue) {
            threshold = i;
            minvalue = J;
        }
    }

    return threshold;
}

int Binarization::fuzzyMinimumError(QImage &image)
{
    int threshold = 0;
    qreal mu0, mu1, e;
    qreal mine = std::numeric_limits<qreal>::max();
    qreal max = 0, min = 255;
    qreal C = max - min;

    QVector<int> histogram = makeHistoram(image);
    QVector<qreal> fhistogram;

    qreal pixelCount = image.width() * image.height();

    for (int i = 0; i < 256; i++) {
        fhistogram << (qreal) histogram[i] / pixelCount;

        if (histogram[i] > 0) {
            if (i > max)
                max = i;

            if (i < min)
                min = i;
        }
    }

    C = max - min;

    for (int t = 0; t < 255; t++) {
        mu0 = 0;
        qreal c = 0;

        for (int i = 0; i <= t; i++) {
            mu0 += i * fhistogram[i];
            c += fhistogram[i];
        }
        mu0 /= c;

        mu1 = 0;
        c = 0;

        for (int i = t + 1; i < 256; i++) {
            mu1 += i * fhistogram[i];
            c += fhistogram[i];
        }
        mu1 /= c;

        e = 0;
        for (int i = 0; i <= t; i++) {
            e += shannon(C / (C + qFabs(i - mu0))) * histogram[i];
        }

        for (int i = t + 1; i < 256; i++) {
            e += shannon(C / (C + qFabs(i - mu1))) * histogram[i];
        }
        e /= pixelCount;

        if (e < mine) {
            threshold = t;
            mine = e;
        }
    }

    return threshold;
}

qreal Binarization::getLog(qreal f)
{
    if (f <= 0)
        return 0;
    else
        return qLn(f);
}

qreal Binarization::shannon(qreal x)
{
    return -x * getLog(x) - (1 - x) * getLog(1 - x);
}
