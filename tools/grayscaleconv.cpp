#include "grayscaleconv.h"

GrayscaleConv::GrayscaleConv(QObject *parent) : QObject(parent)
{

}

QImage GrayscaleConv::toGrayscale1(QImage &image)
{
    for (int row = 0; row < image.height(); row++)
        for (int col = 0; col < image.width(); col++)
        {
            QColor pixel = image.pixelColor(col, row);
            qreal avg = (pixel.red() + pixel.green() + pixel.blue()) / 3.0;
            pixel.setRgb(avg, avg, avg);
            image.setPixelColor(col, row, pixel);
        }
    return image;
}
