#ifndef LINEARFILTER_H
#define LINEARFILTER_H

#include <QObject>
#include <QImage>
#include <QVector>

class LinearFilter : public QObject
{
    Q_OBJECT
public:
    explicit LinearFilter(QObject *parent = nullptr);
    static QImage filter(QImage &image, QVector<QVector<int>> mask);

    static const QVector<QVector<int>> SMOOTHING_3X3;
    static const QVector<QVector<int>> SHARPENING_3X3;
    static const QVector<QVector<int>> GAUSS_3X3;
    static const QVector<QVector<int>> SOBEL_3X3;


signals:

public slots:
};

#endif // LINEARFILTER_H
