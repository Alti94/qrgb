#ifndef HISTOGRAMOPER_H
#define HISTOGRAMOPER_H

#include <QObject>
#include <QVector>
#include <QImage>

class HistogramOper : public QObject
{
    Q_OBJECT
public:
    explicit HistogramOper(QObject *parent = nullptr);
    static QImage stretch(QImage &image, int min, int max);
    static QImage equation(QImage &image);

signals:

public slots:

private:
    struct Histogram {
        QVector<int> r;
        QVector<int> g;
        QVector<int> b;
    };

    static Histogram makeHistogram(QImage &image);
};

#endif // HISTOGRAMOPER_H
