#include "rectangle.h"

#include <QPainter>
#include <QtDebug>

Rectangle::Rectangle(int x, int y)
{
    this->x = x;
    this->y = y;
}

void Rectangle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPen pen(Qt::RoundCap);
    pen.setWidth(this->lineWidth);
    pen.setColor(this->color);
    painter->setPen(pen);
    QBrush brush;
    if (this->fill)
        brush = QBrush(this->color);
    else
        brush = QBrush();
    painter->setBrush(brush);

    painter->drawRect(this->x, this->y, this->width, this->height);
}

QRectF Rectangle::boundingRect() const
{
    qreal penWidth = 1;
    return QRectF(this->x, this->y, this->width, this->height).normalized();
}

void Rectangle::resize(int width, int height)
{
    this->width = width - this->x;
    this->height = height - this->y;
}
