#include "beziermodifierpoint.h"

BezierModifierPoint::BezierModifierPoint(const QPointF &point, BezierCurve *parent) :
    QGraphicsRectItem(point.x() - 2, point.y() - 2, 5, 5, parent),
    position(QPointF(point))
{
    setPen(QPen(Qt::red));
    setBrush(QBrush(Qt::red));
    setZValue(1000);
    setFlag(QGraphicsItem::ItemIsMovable, true);
}

void BezierModifierPoint::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsRectItem::mouseMoveEvent(event);
    static_cast<BezierCurve*>(parentItem())->modPoint();
}

QPointF BezierModifierPoint::getPos()
{
    return position + pos();
}
