#ifndef BEZIERCURVE_H
#define BEZIERCURVE_H

#include <QGraphicsItem>
#include <QGraphicsLineItem>
#include <QVector>
#include <QPointF>
#include <QPen>
#include <QBrush>

class BezierModifierPoint;

class BezierCurve : public QGraphicsItem
{
public:
    BezierCurve(int x, int y);
    BezierCurve(const QPointF &point);
    ~BezierCurve();

    void addPoint(int x, int y);
    void addPoint(QPointF point);
    void modPoint();
    void removePoint(int pointNum);

    void setEditVisible(bool editVisible);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;

    enum { Type = UserType + 4 };
    int type() const
    {
        return Type;
    }

private:
    QVector<BezierModifierPoint*> guidePoints;
    QVector<QGraphicsLineItem*> guideLines;
    QVector<QPointF> curve;

    qreal interval = 0.001;
    QRectF rect;

    QPen pointPen;
    QBrush pointBrush;
    QPen guideLinePen;

    bool editVisible;

    void update();
    void updateRect();
    int combinations(int n, int k);
    int strong(int k);
};

#endif // BEZIERCURVE_H
