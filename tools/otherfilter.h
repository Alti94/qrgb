#ifndef OTHERFILTER_H
#define OTHERFILTER_H

#include <QObject>
#include <QImage>

class OtherFilter : public QObject
{
    Q_OBJECT
public:
    explicit OtherFilter(QObject *parent = nullptr);
    static QImage medianFilter(QImage &image, int windowWidth, int windowHeight);
signals:

public slots:
};

#endif // OTHERFILTER_H
