#ifndef POINTTRANSFORM_H
#define POINTTRANSFORM_H

#include <QObject>
#include <QImage>

class PointTransform : public QObject
{
    Q_OBJECT
public:
    //explicit PointTransform(QObject *parent = nullptr);
    static QImage transform(QImage &img, char oper, qreal val, bool red, bool green, bool blue);

private:
    explicit PointTransform(QObject *parent = nullptr);
    static QImage addRemove(QImage &img, qreal val, bool red, bool green, bool blue);
    static QImage multiplyDivide(QImage &img, qreal val, bool red, bool green, bool blue);
    static QImage lutApply(QImage img, qint32 (&lut)[256], bool red, bool green, bool blue);

signals:

public slots:
};

#endif // POINTTRANSFORM_H
