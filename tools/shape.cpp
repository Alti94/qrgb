#include "shape.h"

Shape::Shape(QGraphicsItem *parent) : QGraphicsItem(parent)
{

}

void Shape::setColor(QColor &color)
{
    this->color = color;
}

void Shape::setLineWidth(qreal width)
{
    this->lineWidth = width;
}

void Shape::setFilled(bool fill)
{
    this->fill = fill;
}

QColor Shape::getColor()
{
    return this->color;
}

qreal Shape::getLineWidth()
{
    return this->lineWidth;
}

bool Shape::isFilled()
{
    return this->fill;
}
