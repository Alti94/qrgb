#include "linearfilter.h"

LinearFilter::LinearFilter(QObject *parent) : QObject(parent)
{

}

QImage LinearFilter::filter(QImage &image, QVector<QVector<int>> mask)
{
    QImage after = image;

    int maskX = mask[0].size();
    int maskY = mask.size();

    int maskSum = 0;

    for (int i = 0; i < maskX; i++)
        for (int j = 0; j < maskY; j++)
            maskSum += mask[i][j];

    if (maskSum <= 0)
        maskSum = 1;

    int maskHalfX = maskX / 2;
    int maskHalfY = maskY / 2;

    for (int y = 0; y < image.height(); y++)
        for (int x = 0; x < image.width(); x++)
        {
            int red = 0, green = 0, blue = 0;
            for (int j = 0; j < maskY; j++)
                for (int i = 0; i < maskX; i++)
                {
                    int xx = x + i - maskHalfX;
                    int yy = y + j - maskHalfY;
                    xx = (xx < 0) ? 0 : ((xx >= image.width()) ? image.width() - 1 : xx);
                    yy = (yy < 0) ? 0 : ((yy >= image.height()) ? image.height() - 1 : yy);
                    QColor color = image.pixelColor(xx ,yy);
                    red += color.red() * mask[j][i];
                    green += color.green() * mask[j][i];
                    blue += color.blue() * mask[j][i];
                }
            red /= maskSum;
            green /= maskSum;
            blue /= maskSum;

            if (red < 0) red = 0;
            if (red > 255) red = 255;
            if (green < 0) green = 0;
            if (green > 255) green = 255;
            if (blue < 0) blue = 0;
            if (blue > 255) blue = 255;

            after.setPixelColor(x, y, QColor(red, green, blue));
        }

    return after;
}

const QVector<QVector<int>> LinearFilter::SMOOTHING_3X3 = {
    QVector<int>({1, 1, 1}),
    QVector<int>({1, 1, 1}),
    QVector<int>({1, 1, 1}),
};

const QVector<QVector<int>> LinearFilter::SHARPENING_3X3 = {
    QVector<int>({-1, -1, -1}),
    QVector<int>({-1,  9, -1}),
    QVector<int>({-1, -1, -1}),
};

const QVector<QVector<int>> LinearFilter::GAUSS_3X3 = {
    QVector<int>({1, 2, 1}),
    QVector<int>({2, 4, 2}),
    QVector<int>({1, 2, 1}),
};

const QVector<QVector<int>> LinearFilter::SOBEL_3X3 = {
    QVector<int>({ 1,  2,  1}),
    QVector<int>({ 0,  0,  0}),
    QVector<int>({-1, -2, -1}),
};
