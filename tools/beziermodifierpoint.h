#ifndef BEZIERMODIFIERPOINT_H
#define BEZIERMODIFIERPOINT_H

#include <QGraphicsRectItem>
#include "beziercurve.h"

class BezierModifierPoint : public QGraphicsRectItem
{
public:
    BezierModifierPoint(const QPointF &point, BezierCurve *parent);
    QPointF getPos();

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
private:
    QPointF position;
    int id;
};

#endif // BEZIERMODIFIERPOINT_H
