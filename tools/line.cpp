#include "line.h"

#include <QPainter>
#include <QtDebug>

Line::Line(int x, int y)
{
    this->x = x;
    this->y = y;
    this->width = x;
    this->height = y;
}

void Line::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPen pen(Qt::RoundCap);
    pen.setWidth(this->lineWidth);
    pen.setColor(this->color);
    painter->setPen(pen);
    painter->drawLine(this->x, this->y, this->width, this->height);
}

QRectF Line::boundingRect() const
{
    return QRectF(QPointF(this->x, this->y), QPointF(this->width, this->height)).normalized();
}

void Line::resize(int width, int height)
{
    this->width = width;
    this->height = height;
}
