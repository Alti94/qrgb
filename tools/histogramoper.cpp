#include "histogramoper.h"

HistogramOper::HistogramOper(QObject *parent) : QObject(parent)
{

}

HistogramOper::Histogram HistogramOper::makeHistogram(QImage &image)
{
    Histogram histogram;

    for (int i = 0; i < 256; i++)
    {
        histogram.r << 0;
        histogram.g << 0;
        histogram.b << 0;
    }

    for (int row = 0; row < image.height(); row++)
        for (int col = 0; col < image.width(); col++)
        {
            QColor color = image.pixelColor(col, row);
            histogram.r[color.red()] += 1;
            histogram.g[color.green()] += 1;
            histogram.b[color.blue()] += 1;
        }

    return histogram;
}

QImage HistogramOper::stretch(QImage &image, int min, int max)
{
//    Histogram hGram = makeHistogram(image);

//    int rMax = 0, rMin = INT32_MAX,
//            gMax = 0, gMin = INT32_MAX,
//            bMax = 0, bMin = INT32_MAX;

//    for (int i = 0; i < 256; i++)
//    {
//        if (rMax < hGram.r[i])
//            rMax = hGram.r[i];
//        if (gMax < hGram.g[i])
//            gMax = hGram.g[i];
//        if (bMax < hGram.b[i])
//            bMax = hGram.b[i];

//        if (rMin > hGram.r[i])
//            rMin = hGram.r[i];
//        if (gMin > hGram.g[i])
//            gMin = hGram.g[i];
//        if (bMin > hGram.b[i])
//            bMin = hGram.b[i];
//    }

    QVector<int> lut;
    for (int i = 0; i < 256; i++)
    {
        int val = ((qreal)255 / (qreal) (max - min)) * (i - min);

        if (val > 255) val = 255;
        if (val < 0) val = 0;

        lut << val;
    }

    for (int row = 0; row < image.height(); row++)
        for (int col = 0; col < image.width(); col++)
        {
            QColor color = image.pixelColor(col, row);
            color.setRed(lut[color.red()]);
            color.setGreen(lut[color.green()]);
            color.setBlue(lut[color.blue()]);

            image.setPixelColor(col, row, color);
        }

    return image;
}

QImage HistogramOper::equation(QImage &image)
{
    Histogram histogram = makeHistogram(image);

    qreal sumRed = 0, sumGreen = 0, sumBlue = 0;
    qreal dRed[256], dGreen[256], dBlue[256];
    qreal d0RedMin = 0, d0GreenMin = 0, d0BlueMin = 0;
    qreal pixelCount = image.width() * image.height();

    for (int i = 0; i < 256; i++)
    {
        sumRed += histogram.r[i] / pixelCount;
        sumGreen += histogram.g[i] / pixelCount;
        sumBlue += histogram.b[i] / pixelCount;

        dRed[i] = sumRed;
        dGreen[i] = sumGreen;
        dBlue[i] = sumBlue;

        if (dRed[i] > 0 && d0RedMin == 0) d0RedMin = dRed[i];
        if (dGreen[i] > 0 && d0GreenMin == 0) d0GreenMin = dGreen[i];
        if (dBlue[i] > 0 && d0BlueMin == 0) d0BlueMin = dBlue[i];
    }

    int lutRed[256], lutGreen[256], lutBlue[256];

    for (int i = 0; i < 256; i++) {
        lutRed[i] = (int) (((dRed[i] - d0RedMin) / (1 - d0RedMin)) * 255);
        lutGreen[i] = (int) (((dGreen[i] - d0GreenMin) / (1 - d0GreenMin)) * 255);
        lutBlue[i] = (int) (((dBlue[i] - d0BlueMin) / (1 - d0BlueMin)) * 255);
    }


    for (int row = 0; row < image.height(); row++)
        for (int col = 0; col < image.width(); col++)
        {
            QColor color = image.pixelColor(col, row);
            color.setRed(lutRed[color.red()]);
            color.setGreen(lutGreen[color.green()]);
            color.setBlue(lutBlue[color.blue()]);

            image.setPixelColor(col, row, color);
        }

    return image;
}
