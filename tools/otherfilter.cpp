#include "otherfilter.h"

OtherFilter::OtherFilter(QObject *parent) : QObject(parent)
{

}

QImage OtherFilter::medianFilter(QImage &image, int windowWidth, int windowHeight)
{
    QImage imageAfter = image;



    int windowHalfWidth = windowWidth / 2;
    int windowHalfHeight = windowHeight / 2;

    int wMaxWidth = windowWidth - windowHalfWidth;
    int wMaxHeight = windowHeight - windowHalfHeight;

    int half = (windowWidth * windowHeight) / 2;

    for (int y = 0; y < image.height(); y++)
        for (int x = 0; x < image.width(); x++)
        {
            QVector<int> vectorRed, vectorGreen, vectorBlue;
            for (int j = -windowHalfHeight; j < wMaxHeight; j++)
                for (int i = -windowHalfWidth; i < wMaxWidth; i++)
                {
                    int xx = x + i;
                    int yy = y + j;
                    QColor color = image.pixelColor(
                                (xx < 0) ? 0 : ((xx >= image.width()) ? image.width() - 1 : xx),
                                (yy < 0) ? 0 : ((yy >= image.height()) ? image.height() - 1 : yy)
                    );

                    vectorRed.append(color.red());
                    vectorGreen.append(color.green());
                    vectorBlue.append(color.blue());
                }

            std::sort(vectorRed.begin(), vectorRed.end());
            std::sort(vectorGreen.begin(), vectorGreen.end());
            std::sort(vectorBlue.begin(), vectorBlue.end());

            imageAfter.setPixelColor(x, y, QColor(vectorRed.at(half), vectorGreen.at(half), vectorBlue.at(half)));
        }

    return imageAfter;
}
