#ifndef ELIPSE_H
#define ELIPSE_H

#include "tools/shape.h"

class Elipse : public Shape
{
public:
    enum { Type = UserType + 3 };
    Elipse(int x, int y);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
    void resize(int width, int height);

    int type() const
    {
        return Type;
    }

private:
};

#endif // ELIPSE_H
