#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "tools/shape.h"

class Rectangle : public Shape
{
public:
    enum { Type = UserType + 2 };
    Rectangle(int x, int y);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
    void resize(int width, int height);

    int type() const
    {
        return Type;
    }

private:
};

#endif // RECTANGLE_H
