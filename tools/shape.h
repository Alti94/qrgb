#ifndef SHAPE_H
#define SHAPE_H

#include <QObject>
#include <QGraphicsItem>

class Shape : public QGraphicsItem
{
public:
    void virtual resize(int width, int height) = 0;
    void setColor(QColor &color);
    void setLineWidth(qreal width = 1);
    void setFilled(bool fill = false);
    QColor getColor();
    qreal getLineWidth();
    bool isFilled();

protected:
    Shape(QGraphicsItem *parent = Q_NULLPTR);
    QColor color = QColor::fromRgb(0, 0, 0);
    qreal lineWidth = 1;
    bool fill = false;
    int x;
    int y;
    int width = 1;
    int height = 1;
};

#endif // SHAPE_H
