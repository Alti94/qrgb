#ifndef CANVAS_H
#define CANVAS_H

#include <QWidget>
#include <QGraphicsView>

class Canvas : public QGraphicsView
{
    Q_OBJECT
public:
    explicit Canvas(QWidget *parent = nullptr);

signals:
    void mouseMoved(QMouseEvent *event);
    void mousePressed(QMouseEvent *event);

public slots:

protected:
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
};

#endif // CANVAS_H
