#include "customwindowscreator.h"
#include "ui_customwindowscreator.h"

#include <QGridLayout>
#include <QSpinBox>

CustomWindowsCreator::CustomWindowsCreator(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CustomWindowsCreator)
{
    ui->setupUi(this);

    for (int i = 0; i < rows; i++)
        for (int j = 0; j < cols; j++)
        {
            QSpinBox *spin = new QSpinBox(this);
            spin->setRange(-100, 100);
            spin->setValue(0);
            ui->grid_spinWindow->addWidget(spin, i, j);
        }
}

CustomWindowsCreator::~CustomWindowsCreator()
{
    delete ui;
}

void CustomWindowsCreator::on_button_colAdd_clicked()
{
    for (int i = 0; i < rows; i++)
    {
        QSpinBox *spin = new QSpinBox(this);
        spin->setRange(-100, 100);
        spin->setValue(0);
        ui->grid_spinWindow->addWidget(spin, i, cols);
    }

    cols++;
}

void CustomWindowsCreator::on_button_rowAdd_clicked()
{
    for (int i = 0; i < cols; i++)
    {
        QSpinBox *spin = new QSpinBox(this);
        spin->setRange(-100, 100);
        spin->setValue(0);
        ui->grid_spinWindow->addWidget(spin, rows, i);
    }

    rows++;
}

void CustomWindowsCreator::on_button_colRemove_clicked()
{
    if (cols > 2)
    {
        cols--;
        for (int i = 0; i < rows; i++)
        {
            QWidget *widget = ui->grid_spinWindow->itemAtPosition(i, cols)->widget();

            if (widget != nullptr)
            {
                ui->grid_spinWindow->removeWidget(widget);
                delete widget;
            }
        }
    }
}

void CustomWindowsCreator::on_button_rowRemove_clicked()
{
    if (rows > 2)
    {
        rows--;
        for (int i = 0; i < cols; i++)
        {
            QWidget *widget = ui->grid_spinWindow->itemAtPosition(rows, i)->widget();

            if (widget != nullptr)
            {
                ui->grid_spinWindow->removeWidget(widget);
                delete widget;
            }
        }
    }
}

void CustomWindowsCreator::on_buttonBox_accepted()
{
    QVector<QVector<int>> mask;
    for (int row = 0; row < rows; row++)
    {
        QVector<int> maskRow;
        for (int col = 0; col < cols; col++)
        {
            QSpinBox *spinBox = qobject_cast<QSpinBox*>(ui->grid_spinWindow->itemAtPosition(row, col)->widget());
            maskRow.append(spinBox->value());
        }
        mask.append(maskRow);
    }

    emit maskComplete(mask);
}
